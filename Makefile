
.PHONY: runDay01 runDay02 runDay04 runDay06

day%:
	cd cmd/$@ && go build

runDay01: day01
	./cmd/day01/day01

runDay02: day02
	./cmd/day02/day02

runDay04: day04
	./cmd/day04/day04

runDay06: day06
	./cmd/day06/day06

runDay08: day08
	./cmd/day08/day08 data/day08.txt