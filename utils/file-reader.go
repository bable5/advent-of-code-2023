package utils

import (
	"os"
	"strings"
)

func ReadFile(path string) []string {
	data, err := os.ReadFile(path)
	check(err)

	asString := string(data)

	return strings.Split(asString, "\n")
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}
