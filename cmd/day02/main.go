package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/bable5/advent-of-code-2023/utils"
)

type game struct {
	number int
	pulls  []gameValues
}

type gameValues struct {
	count int
	color string
}

const MAX_RED = 12
const MAX_GREEN = 13
const MAX_BLUE = 14

func parse(s string) (game, error) {
	parts := strings.Split(s, ":")

	number, err := strconv.Atoi(strings.Split(parts[0], " ")[1])
	if err != nil {
		return game{}, err
	}

	//TODO: This naming is confusing
	game := game{
		number: number,
	}

	for _, g := range strings.Split(strings.TrimSpace(parts[1]), ";") {
		g = strings.TrimSpace(g)
		pulls := strings.Split(g, ",")
		//TODO: We're not modeling the grouping by ';'. It flattens to the the whole 'game'

		for _, p := range pulls {
			p = strings.TrimSpace((p))
			parts := strings.Split(p, " ")
			count, err := strconv.Atoi(parts[0])
			if err != nil {
				return game, err
			}

			v := gameValues{
				count,
				parts[1],
			}

			game.pulls = append(game.pulls, v)
		}
	}

	return game, nil
}

func filterToValidGames(games []game) []game {
	valid := make([]game, 0)
	for _, g := range games {
		isValid := true
		for _, p := range g.pulls {
			if p.color == "red" && p.count > MAX_RED {
				isValid = false
				break
			} else if p.color == "green" && p.count > MAX_GREEN {
				isValid = false
				break
			} else if p.color == "blue" && p.count > MAX_BLUE {
				isValid = false
				break
			}
		}
		if isValid {
			valid = append(valid, g)
		}
	}
	return valid
}

func part1ValidGames(games []game) {

	validGames := filterToValidGames(games)
	sum := 0
	for _, g := range validGames {
		sum = sum + g.number
	}
	fmt.Println(sum)
}

func part2PowerSet(games []game) {
	sum := 0
	for _, g := range games {
		minRed := 0
		minBlue := 0
		minGreen := 0

		for _, p := range g.pulls {
			if p.color == "red" && p.count > minRed {
				minRed = p.count
			} else if p.color == "green" && p.count > minGreen {
				minGreen = p.count
			} else if p.color == "blue" && p.count > minBlue {
				minBlue = p.count
			}
		}

		gamePower := minRed * minGreen * minBlue
		sum += gamePower
	}
	fmt.Println(sum)
}

func main() {
	input := utils.ReadFile("./data/day02.txt")
	games := make([]game, 0)
	for _, s := range input {
		g, err := parse(s)
		if err != nil {
			panic(err)
		}
		games = append(games, g)
	}

	part1ValidGames(games)
	part2PowerSet(games)
}
