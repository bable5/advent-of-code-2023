package main

import (
	"fmt"

	"gitlab.com/bable5/advent-of-code-2023/utils"
)

func main() {
	input := utils.ReadFile("./data/day01.txt")
	for _, s := range input {
		fmt.Println(s)
	}
}
