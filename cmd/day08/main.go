package main

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/bable5/advent-of-code-2023/utils"
)

type Node struct {
	left  string
	right string
}

func (n Node) step(direction string) string {
	if direction == "L" {
		return n.left
	} else if direction == "R" {
		return n.right
	} else {
		panic(fmt.Sprintf("Unknown direction %s", &direction))
	}
}

type NetworkMap struct {
	directions []string
	nodes      map[string]Node
}

func findPath(networkMap NetworkMap, src string, tgt string) []string {
	directionIdx := 0

	steps := make([]string, 0)

	current := networkMap.nodes[src]
	for {
		dir := networkMap.directions[directionIdx]
		next := current.step(dir)
		steps = append(steps, next)
		if next == tgt {
			break
		}
		current = networkMap.nodes[next]
		directionIdx = (directionIdx + 1) % len(networkMap.directions)
	}

	return steps
}

func parse(input []string) NetworkMap {
	networkMap := NetworkMap{
		nodes: make(map[string]Node),
	}

	networkMap.directions = strings.Split(input[0], "")

	for _, s := range input[2:] {
		parts := strings.Split(s, " = ")
		target := parts[0]
		dest := strings.Split(parts[1][1:len(parts[1])-1], ", ")
		node := Node{
			dest[0],
			dest[1],
		}
		networkMap.nodes[target] = node
	}

	return networkMap
}

func main() {
	if len(os.Args) < 2 {
		fmt.Println("usage day08 <path/to/data>")
		os.Exit(1)
	}

	datafile := os.Args[1]

	input := utils.ReadFile(datafile)

	networkMap := parse(input)
	steps := findPath(networkMap, "AAA", "ZZZ")

	fmt.Println(len(steps))
}
