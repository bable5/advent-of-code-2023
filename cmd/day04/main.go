package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/bable5/advent-of-code-2023/utils"
)

type ScratchCard struct {
	cardNumber int
	numbers    []int
	winning    []int
}

func parse(input []string) ([]ScratchCard, error) {
	res := make([]ScratchCard, 0)
	for _, l := range input {
		p1 := strings.Split(l, ":")

		cards := strings.Fields(p1[0])
		cardNumber, err := strconv.Atoi(strings.TrimSpace(cards[1]))
		if err != nil {
			return res, err
		}

		numbers := strings.Split(p1[1], " | ")
		winningNumbers, err := parseNumberList(numbers[0])
		if err != nil {
			return res, err
		}

		myNumbers, err := parseNumberList(numbers[1])
		if err != nil {
			return res, err
		}

		res = append(res, ScratchCard{
			cardNumber: cardNumber,
			numbers:    myNumbers,
			winning:    winningNumbers,
		})
	}
	return res, nil
}

func parseNumberList(input string) ([]int, error) {
	res := make([]int, 0)
	for _, s := range strings.Fields(input) {
		i, err := strconv.Atoi(s)

		if err != nil {
			return res, err
		}

		res = append(res, i)
	}
	return res, nil
}

func contains(i int, vs []int) bool {
	for _, v := range vs {
		if i == v {
			return true
		}
	}
	return false
}

func main() {
	input := utils.ReadFile("./data/day04.txt")

	scratchCards, err := parse(input)
	if err != nil {
		panic(err)
	}

	sum := 0
	for _, card := range scratchCards {
		score := 0
		for _, n := range card.numbers {
			if contains(n, card.winning) {
				if score == 0 {
					score = 1
				} else {
					score = score * 2
				}
			}
		}
		sum += score
	}

	fmt.Println(sum)
}
