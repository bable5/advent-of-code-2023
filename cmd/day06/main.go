package main

import (
	"fmt"
	"strconv"
	"strings"

	"gitlab.com/bable5/advent-of-code-2023/utils"
)

type Race struct {
	time   int
	record int
}

func parse(inputFile string) []Race {
	input := utils.ReadFile(inputFile)

	times := strings.Fields(input[0])
	records := strings.Fields(input[1])

	res := make([]Race, 0)

	for i := 1; i < len(times); i++ {
		time, err := strconv.Atoi(times[i])
		if err != nil {
			panic(err)
		}
		record, err := strconv.Atoi(records[i])
		if err != nil {
			panic(err)
		}
		res = append(res, Race{time: time, record: record})
	}

	return res
}

func part1(input []Race) int {
	res := 1
	for _, r := range input {
		count := 0
		// fmt.Println(r)
		for i := 0; i < r.time; i++ {
			pushTime := i
			runTime := r.time - pushTime

			distance := pushTime * runTime

			if distance > r.record {
				// fmt.Printf("Push for %d to win\n", pushTime)
				count++
			}
		}

		// fmt.Printf("Race %v can be won %d ways\n ", r, count)
		if count > 0 {
			res = res * count
		}
	}
	return res
}

func part2(inputFile string) int {
	input := utils.ReadFile(inputFile)

	times := strings.Fields(input[0])[1:]
	timeStr := ""
	for _, s := range times {
		timeStr = timeStr + s
	}
	time, err := strconv.Atoi(timeStr)
	if err != nil {
		panic(err)
	}

	distances := strings.Fields(input[1])[1:]
	distanceStr := ""
	for _, s := range distances {
		distanceStr = distanceStr + s
	}
	distance, err := strconv.Atoi(distanceStr)
	if err != nil {
		panic(err)
	}

	race := []Race{{time, distance}}
	fmt.Println(race)

	return part1(race)
}

func main() {
	inputFile := "data/day06.txt"
	races := parse(inputFile)

	fmt.Println(part1(races))
	fmt.Println(part2(inputFile))
}
